"use strict" ;

// 1. Як можна створити рядок у JavaScript?

// Рядок можна створити за допомогою лапок :одинарних (''),подвійних (""),зворотніх (``).


// 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?

// Одинарні лапки та подвійні лапки вони ідентичні за значенням, використовуються для оголошення рядків.
// Зворотні лапки вони використовуються для створення шаблонних рядків, в шаблонні рядки можна включати вирази $()


// 3. Як перевірити, чи два рядки рівні між собою?

// Для цієї задачі використовуємо оператор порівняння (===)


// 4. Що повертає Date.now()?

// Повертає кількість мілісекунд, які пройшли з 1 січня 1970 року


// 5. Чим відрізняється Date.now() від new Date()?

// Date.now() повертає кількість мілісекунд , new Date() cтворює новий об'єкт, представляючи поточний момент часу.


// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

// function isPalindrome(str){
//     const strNew = str.replace(/[\W_]/g, '').toLowerCase();

//     const strOld = strNew.split('').reverse().join('');

//     return strNew === strOld;
// }
// console.log(isPalindrome("радар"));
// console.log(isPalindrome("Козак з казок"))


// 2. Створіть функцію, яка перевіряє довжину рядка.
// Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. 
// Ця функція стане в нагоді для валідації форми.

// function isValidLength (str,maxLength) {
//     const stringLength = str.length ;
//     return stringLength <= maxLength ;
// }
// console.log(isValidLength('check string',20));


// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
//  Функція повина повертати значення повних років на дату виклику функцію.

// function exactlyAge() {
//     let userAgeInput = prompt(`Enter your date of birth in year-month-day format:`);
//     let userAgeBirth = new Date(userAgeInput);
//     let nowDate = new Date();
//     let age = nowDate.getFullYear() - userAgeBirth.getFullYear();
//     if (nowDate.getMonth() < userAgeBirth.getMonth() ||
//         (nowDate.getMonth() === userAgeBirth.getMonth() && nowDate.getDate() < userAgeBirth.getDate())) {
//       age--;
//     }
//     return `You are currently  ${age} years old.`;
//   }
//   console.log(exactlyAge());